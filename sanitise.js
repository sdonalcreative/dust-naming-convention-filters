/*jslint node: true */
'use strict';

function getUnderscoreWords(value) {
    return value.split('_');
}

function getHyphenWords(value) {
    return value.split('-');
}

function getSpacedWords(value) {
    return value.split(' ');
}

function getCasedWords(value) {
    return getSpacedWords(value.replace(/([a-z0-9])([A-Z])/, '$1 $2'));
}

var sanitisers = [
    getUnderscoreWords,
    getHyphenWords,
    getSpacedWords,
    getCasedWords
];

module.exports = function (value) {
    var i, words, sanitised;

    value = value.trim();

    for (i = 0; i < sanitisers.length; i += 1) {
        sanitised = sanitisers[i](value).filter(Boolean);

        if (sanitised.length > 1) {
            words = sanitised;
            break;
        }
    }

    if (!words) {
        words = [ value ];
    }

    words = words.map(function (value) {
        return value.toLowerCase();
    });

    return words;
};
