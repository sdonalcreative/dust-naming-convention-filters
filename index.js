/*jslint node: true */
'use strict';

var filters = [
    'camel',
    'hyphenate',
    'pascal',
    'underscore'
];

module.exports = function (dust) {
    filters.forEach(function (filter) {
        dust.filters[filter] = require('./filters/' + filter);
    });
};
