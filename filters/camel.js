/*jslint node: true */
'use strict';

var sanitise = require('../sanitise');

module.exports = function (value) {
    var i, words = sanitise(value);

    value = '';

    for (i = 0; i < words.length; i += 1) {
        if (i === 0) {
            value += words[i];
        } else {
            value += words[i].charAt(0).toUpperCase() + words[i].slice(1);
        }
    }

    return value;
};
