/*jslint node: true */
'use strict';

var sanitise = require('../sanitise');

module.exports = function (value) {
    var i, words = sanitise(value);

    value = '';

    for (i = 0; i < words.length - 1; i += 1) {
        value += words[i] + '-';
    }

    value += words[words.length - 1];

    return value;
};
