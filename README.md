# Dust.js Naming Convention Filters

Intended for converting between different naming conventions easily.

For example: `MyAwesomeFoo` > `my-awesome-foo`.

## Usage
```javascript
    var dust = require('dustjs-linkedin');
    require('dust-naming-convention-filters')(dust);
```

## Filters

None of these filters preserve any of the original casing.

For the below filters, any of the following strings will produce the same output: `MyAwesomeFoo`, `mY AwEsome FOO`, `my-AWESOME-foo`, `my_awesome_foo`

### Camel Casing: "myAwesomeFoo"
Usage: `{myDustPath|camel}`

### Hyphen Separated: "my-awesome-foo"
Usage: `{myDustPath|hyphenate}`

### Pascal Casing: "MyAwesomeFoo"
Usage: `{myDustPath|pascal}`

### Underscore Separated: "my_awesome_foo"
Usage: `{myDustPath|underscore}`
